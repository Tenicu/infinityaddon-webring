# InfinityAddon-Webring

Addon enabling the collaborating with other imageboards to form a decentralized webring via a standardized schema.

This addon is compatible with vichan-based imageboards, including vichan, infinity, OpenIB, and NPFchan. For use of the webring system with LynxChan, see https://gitlab.com/alogware/LynxChanAddon-Webring and follow the directions there. 

## The Webring Schema

The schema is identical to that used by the equivalent LynxChan addon https://gitlab.com/alogware/LynxChanAddon-Webring, sites using either system are able to participate in the same webring.

```js
{
    "name": "Imageboard", // The imageboard's name.
    "url": "https://image.board", // The imageboard's base URL.
    "endpoint": "https://image.board/webring.json", // The current endpoint.

    "following": [
        // A list of imageboards that this imageboard is initially pairing with.
        // The system will attempt to recursively follow the links for followed sites and build 
        // a complete picture of the webring.
        "https://another.imageboard/webring.json",
        "https://yet.another.ib/webring.json"
    ],

    "known": [
        // A list of imageboards this imageboard pairs with, but doesn't necessarily
        // follow directly.
        "https://another.imageboard/webring.json",
        "https://yet.another.ib/webring.json",
        "https://some.other.board/webring.json"
    ],

    "blacklist": [ // A list of domains that this imageboard will not pair with.
        "bad.imageboard",
        "illegal.ib"
    ],
    
    "logo": [ // A list of logos for this imageboard. Should not be more than 300KB.
        "https://image.board/static/logo.png"
    ],
    
    "flags": [ // A list of flags designating site special conditions. Can include things like 'tor' or
               // 'opennic' designating special user configuration required to view the site. Optional.
        "opennic",
        "tor",
        "ipfs"
    ],

    "boards": [
        {
            "uri": "a", // The URI of the board.
            "title": "Anime & Manga", // The board name/title.
            "subtitle": "Discussion about tea and taims", // The board subtitle/description. Optional.
            "path": "https://image.board/a/", // The absolute path for the board.

            "nsfw": true, // Whether this board is marked as not safe for work. Optional.
            "postsPerHour": 23, // The number of posts made in the last hour. Optional.
            "totalPosts": 1000, // The total number of posts on the board. Optional.
            "uniqueUsers": 20, // The total number of unique IPs as reported by the imageboard software. Optional.
            "tags": ["anime", "manga"], // A list of tags used by this board. Maximum of five. Optional.
            "lastPostTimestamp": "2019-10-13T12:00:00+00:00" // Time of last post on board in ISO 8601. Optional.
        }
        // ...
    ]
}
```

## Installation 

1. Clone the repo with `git clone https://gitlab.com/Tenicu/infinityaddon-webring.git`.
2. Move files to your IB's root directory (e.g. `/var/www/html`);
3. Configure `webring/webring-config.php`.
4. Add an entry to your crontab `0,15,30,45 * * * * /usr/bin/php /var/www/html/webring/buildring.php`. You can get to it with `crontab -e`.
5. Add `$config['additional_javascript'][] = 'js/webring.js';` to inc/instance-config.php.
6. In the IB's web interface, visit `https://<yoursite>/mod.php?/rebuild` and rebuild the Javascript files.

## License

This addon is licensed under the GNU General Public License Version 3. &copy; The Collective of Individuals 2019.
