<?php

chdir(dirname(__FILE__));
chdir('..');
chdir(realpath('inc') . '/..');

require_once('webring/webring-config.php');
require_once('inc/functions.php');

$webRingPath = 'webring.json';
$knownHostsPath = 'webring/known_hosts.json';
$outputPath = 'webring/compiled_webring.json';

function dbQuery($query){
    global $pdo, $config, $debug;
    sql_open();
    $result = $pdo->query($query);
    return $result->fetchAll(PDO::FETCH_ASSOC);
}

function getLocalBoards($url){
    global $config;

    // Work out if we're using infinity or derivatives
    $infinity = TRUE; 
    if (count(dbQuery('SHOW COLUMNS FROM ' . $config['db']['prefix'] . 'boards LIKE "indexed"')) == 0){
        $infinity = FALSE;
    }
    
    $base = $infinity ? dbQuery('SELECT * FROM ' . $config['db']['prefix'] . 'boards WHERE indexed IS TRUE') : dbQuery('SELECT * FROM ' . $config['db']['prefix'] . 'boards');
    
    $boards = array();
    for ($i = 0; $i < count($base); $i++){
        $item['postsPerHour'] = (int)(dbQuery('SELECT COUNT(id) FROM ' . $config['db']['prefix'] . 'posts_' . $base[$i]['uri'] . ' WHERE time > UNIX_TIMESTAMP() - 3600')[0]['COUNT(id)']);
        $item['uniqueUsers'] = (int)(dbQuery('SELECT COUNT(DISTINCT ip) FROM ' . $config['db']['prefix'] . 'posts_' . $base[$i]['uri'] . ' WHERE time > UNIX_TIMESTAMP() - 86400')[0]['COUNT(DISTINCT ip)']);
        $item['totalPosts'] = (int)($infinity ? $base[$i]['posts_total'] : dbQuery('SELECT id FROM ' . $config['db']['prefix'] . 'posts_' . $base[$i]['uri'] . ' ORDER BY id DESC LIMIT 1')[0]['id']);
        $item['uri'] = $base[$i]['uri'];
        $item['title'] = $base[$i]['title'];
        $item['subtitle'] = $base[$i]['subtitle'];
        $item['path'] = $url . '/' . $base[$i]['uri'];
        $item['nsfw'] = $infinity ? ($base[$i]['sfw'] == '1' ? FALSE : TRUE) : TRUE;
        $datetime = new DateTime('@' . dbQuery('SELECT time FROM ' . $config['db']['prefix'] . 'posts_' . $base[$i]['uri'] . ' WHERE email NOT LIKE "sage" OR email IS NULL ORDER BY time DESC LIMIT 1')[0]['time']);
        $item['lastPostTimestamp'] = $datetime->format(DateTime::ATOM);
        
        if ($infinity){
            $tags = array();
            $tagArr = dbQuery('SELECT tag FROM ' . $config['db']['prefix'] . 'board_tags WHERE uri LIKE "' . $base[$i]['uri'] . '"');
            for ($j = 0; $j < count($tagArr); $j++){
                $tags[] = $tagArr[$j]['tag'];
            }
            $item['tags'] = $tags;
        }
        
        $boards[] = $item;
    }
    
    return $boards;
}

function isKnownHost($currentHost, $knownHosts){
    for ($j = 0; $j < count($knownHosts); $j++){
        if (!empty(parse_url($currentHost)['host']) && parse_url($currentHost)['host'] == parse_url($knownHosts[$j])['host']){
            return TRUE;
        }
    }
    return FALSE;
}

function isBlacklisted($currentHost, $knownHosts){
    for ($j = 0; $j < count($knownHosts); $j++){
        if (!empty(parse_url($currentHost)['host']) && parse_url($currentHost)['host'] == $knownHosts[$j]){
            return TRUE;
        }
    }
    return FALSE;
}

$knownHostsFile = @file_get_contents($knownHostsPath);

/* Only needed for working out if the blacklist has been updated TODO
$baseJsonFile = file_get_contents($webRingPath);
$baseJson = json_decode($baseJsonFile, TRUE);
if ($baseJson == NULL || $baseJson == FALSE){
    console.log('Missing or malformed webring.json.');
    die();
}*/

//The "known" field is now deprecated for security issues, for legacy reasons it will still be included in the produced webring.json however it will no longer be considered when spidering the webring.
//$knownHosts = @json_decode($knownHostsFile, TRUE);
$knownHosts = NULL;
if ($knownHosts == NULL || $knownHosts == FALSE)
    $knownHosts = array();

for ($i = 0; $i < count($webring['following']); $i++)
    if (!isKnownHost($webring['following'][$i], $knownHosts))
        $knownHosts[] = $webring['following'][$i];

$compiledJson = array();

for ($i = 0; $i < count($knownHosts); $i++){
    $currentRingFile = @file_get_contents($knownHosts[$i]);
    if ($currentRingFile == FALSE)
        continue;
    $currentRingJson = @json_decode($currentRingFile, TRUE);
    if ($currentRingJson == FALSE)
        continue;
    
    //TODO, try getting TOR jsons with curl --socks5 localhost:9050 --socks5-hostname localhost:9050 -s http://bhm5koavobq353j54qichcvzr6uhtri6x4bjjy4xkybgvxkzuslzcqid.onion/webring.json
    
    $compiledJson[] = $currentRingJson;

    for ($j = 0; !empty($currentRingJson['following']) && $j < count($currentRingJson['following']); $j++){
        if (!isKnownHost($currentRingJson['following'][$j], $knownHosts) && !isBlacklisted($currentRingJson['following'][$j], $webring['blacklist'])){
            if ($currentRingJson['following'][$j] != $webring['endpoint']){
                $knownHosts[] = $currentRingJson['following'][$j];
            }
        }
    }
}
$webring['known'] = $knownHosts;
$webring['boards'] = getLocalBoards($webring['url']);

//$compiledJson[] = $webring; TODO insert the webring base at the start

file_put_contents($webRingPath, json_encode($webring));
file_put_contents($knownHostsPath, json_encode($knownHosts));
file_put_contents($outputPath, json_encode($compiledJson));

